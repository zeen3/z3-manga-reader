#!/bin/make
DIST_FOLDER=dist
ESNEXT_EXTENSION=mjs

Makefile: esnext umd #.mkln

# autominifier
# -e "s/^ \+//g"
esnext:
	tsc -t ESNext -m ESNext
	for v in $(DIST_FOLDER)/**/**.js; do \
		sed \
			-e 's/.js.map/.$(ESNEXT_EXTENSION).map/g' \
			-e "s/from '\\(.\/[^']*\\)'/from '\\1.$(ESNEXT_EXTENSION)'/g" \
			-e "s/from '\\(..\/[^']*\\)'/from '\\1.$(ESNEXT_EXTENSION)'/g" \
			-e "s/import(\(['\"]\)\([^\1\]\)\1)/import(\\1\\2.$(ESNEXT_EXTENSION)\\1)/g" \
			-i "$$v" ; \
		mv "$$v" "$$(dirname "$$v")/$$(basename "$$v" .js).$(ESNEXT_EXTENSION)" ; done
	for v in $(DIST_FOLDER)/**/**.js.map; do \
		mv "$$v" "$$(dirname "$$v")/$$(basename "$$v" .js.map).$(ESNEXT_EXTENSION).map" ; done

umd:
	tsc -m umd -t ES2017
	for v in $(DIST_FOLDER)/**/**.js; do \
		sed \
			-e "s/define(/define('$$(basename $$v '.js')', /g" \
			-i "$$v"; \
			done

amd:
	tsc -m amd -t ES2017
	for v in $(DIST_FOLDER)/**/**.js; do \
		sed \
			-e "s/define(/define('$$(basename $$v '.js')', /g" \
			-i "$$v"; \
			done

system:
	tsc -m system -t ES2017

cjs:
	tsc -m commonjs -t ES2017

none:
	tsc -m none 

.mkln:
	for v in $(DIST_FOLDER)/**/**.js $(DIST_FOLDER)/**/**.$(ESNEXT_EXTENSION) $(DIST_FOLDER)/**/**.js.map $(DIST_FOLDER)/**/**.$(ESNEXT_EXTENSION).map; do if [[ -f "$$v" ]]; then ln -sf "$$v" "./$$(basename "$$v")"; fi; done;
	if [[ "$(ESNEXT_EXTENSION)" != "mjs" ]]; then for v in $(DIST_FOLDER)/**/**.$(ESNEXT_EXTENSION); do if [[ -f "$$v" ]]; then ln -sf "$$v" "./$$(basename "$$v" .$(ESNEXT_EXTENSION)).mjs"; ln -sf "$$(basename "$$v")" "$$(dirname "$$v")/$$(basename "$$v" .$(ESNEXT_EXTENSION)).mjs"; fi; done; fi

clrsm:
	if [[ -d dist ]]; then rm -r dist; fi
	symlinks -rd .

include clrsm
