import {keyCodeBindings} from './keyman'
import {ACTIONS} from './actions'
let bindingsHandler: BroadcastChannel | null = new BroadcastChannel('key-code-map-handle')
export const bhm = (): BroadcastChannel => bindingsHandler || (bindingsHandler = new BroadcastChannel('key-code-map-handle'))

// bindings sync
export const isZero:((v:number) => boolean) = v => v === 0
const bindingsHandlerMessage: ((m: MessageEvent) => void) = m => {
	const {code, mod, action} = m.data
	if ('string' !== typeof code ||
		'number' !== typeof mod ||
		'number' !== typeof action)
		throw new TypeError(`key-code-map-handle broadcast channel is only for binding handlers.${
			' '}It does not accept other data types.`)
	if (mod > 15 ||
		mod < 0 ||
		!Number.isInteger(mod))
		throw new RangeError(`modifier parameter must be an integer satisfying 0 <= mod <= 15, recieved ${mod}`)
	if (action < 0 ||
		action >= ACTIONS.NOCONTINUE ||
		!Number.isInteger(action))
		throw new RangeError(`action parameter must be an integer satisfying 0 <= action <= ${
			ACTIONS.NOCONTINUE}, recieved ${action}`)

	if (action !== 0 && !(code in keyCodeBindings))
		keyCodeBindings[code] = new Uint8Array(16)

	if (code in keyCodeBindings) {
		keyCodeBindings[code][mod] = action
		if (action === 0 &&
			keyCodeBindings[code].every(isZero))
			delete keyCodeBindings[code]
	}
}

export const bkdetatch = () => {bhm().onmessage = null}
export const bkattach = () => {bhm().onmessage = bindingsHandlerMessage}


if (document) document.addEventListener('freeze', () => {
	// close unfreezables (channels)
	bindingsHandler = null
}, {capture: true})

export default {
	get: bhm,
	attach: bkattach,
	detatch: bkdetatch
}