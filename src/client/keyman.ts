import {bkattach, bkdetatch, bhm, isZero} from './bhm'
// import nt from './next-tick'
import {openDb, DB, UpgradeDB} from './node_modules/idb/lib/idb'
import {ACTIONS, keyBindings, keyBindingsIDB} from './actions'
import { scrolling, scroller, scrollmove, __scrollmove, scrollhrt } from './scroller'
// Object.freeze(ACTIONS)
interface ActionPerformer {<T extends Event>(action: number, event?: T): void;}
const performAction: ActionPerformer = (action, e) => {
	switch (action) {
		case ACTIONS.SCROLL_PY:
		case ACTIONS.SCROLL_PX:
		case ACTIONS.SCROLL_NY:
		case ACTIONS.SCROLL_NX:
			cancelAnimationFrame(__scrollmove[1])
			if (e instanceof KeyboardEvent && !e.repeat)
				scrollmove[action - ACTIONS.SCROLL_PY] += 1
			else scrollmove[action - ACTIONS.SCROLL_PY] = 1
			if (!scrolling[0] && ++scrolling[0])
				scrollhrt[0] = performance.now()
			__scrollmove[1] = requestAnimationFrame(scroller)
			e && e.preventDefault()
			return
	}
}
interface ActionPerformed {(action: number): void;}
const performedAction: ActionPerformed = n => {
	switch (n) {
		case ACTIONS.SCROLL_PY:
		case ACTIONS.SCROLL_PX:
		case ACTIONS.SCROLL_NY:
		case ACTIONS.SCROLL_NX:
			const scrolldir = n - ACTIONS.SCROLL_PY;
			scrollmove[scrolldir] -= 1
			return
		case ACTIONS.NOCONTINUE:
			scrolling[0] = __scrollmove[0] = 0
			cancelAnimationFrame(__scrollmove[1])
			return
	}
}

export const getModState: ((e: KeyboardEvent) => number) = e => +e.altKey |
	(+e.metaKey << 1) |
	(+e.ctrlKey << 2) |
	(+e.shiftKey << 3);

export const getModName:((n: number) => string[]) = n => {
	const res = []
	if (n & 1) res.push('Alt')
	if (n & 2) res.push('Meta')
	if (n & 4) res.push('Ctrl')
	if (n & 8) res.push('Shift')

	return res
}

// set bindings up (it's an array on window if we have a service worker handling events)
declare var BINDINGS: [string /* code */, number /* mod */, number /* act */][] | undefined;
export const keyCodeBindings: keyBindings = typeof BINDINGS === 'object' ?
	BINDINGS.reduce((o, kw) => {
		if (!(kw[0] in o)) o[kw[0]] = new Uint8Array(16)
		o[kw[0]][kw[1]] = kw[2]
		return o
	}, {} as keyBindings) :
	{};
// we use a `var` when defining
Reflect.deleteProperty(window, 'BINDINGS')
// empty the prototype
Object.setPrototypeOf(keyCodeBindings, null)

document.addEventListener('keyup', e => {
	// early return if nonexistent
	if (!(e.code in keyCodeBindings)) return;
	let i = 16, bk = keyCodeBindings[e.code];
	while (i--) bk[i] && performedAction(bk[i])
	// compact way of saying "for (let i = 16; i--;) if (bk[i]) performedAction(bk[i])"
}, {passive: true, capture: true})
document.addEventListener('keydown', e => {
	if (e.target instanceof HTMLElement && 'value' in e.target) {
		scrolling[0] = 0
		return
	}

	const mod = getModState(e)
	if (!(e.code in keyCodeBindings &&
		keyCodeBindings[e.code][mod] !== ACTIONS.NONE))
		return

	const action = keyCodeBindings[e.code][mod]
	performAction(action, e)
	console.log(
		`keydown code %s mod 0x0%s emitted %O; performed action #%d${'\n'
			}(name: %s, default prevented: %s)`,
		e.code,
		mod.toString(16),
		e,
		action,
		// ACTIONS[action] ||
		'[NOT A REVERSE MAPPED ACTION]',
		e.defaultPrevented
	)
	e.stopImmediatePropagation()
	return
}, {capture: true})

// database sync definite
const blockUnload: ((e: BeforeUnloadEvent) => void) = e => (
	e.preventDefault(),
	e.returnValue = 'Page is currently saving information, really quit?'
)
let _work = 0
const unloadBlock = () => {
	// work == 0; block unload
	// work >  0; don't add more
	_work++ || addEventListener('beforeunload', blockUnload, {capture: true})
	console.assert(_work > 0, `pushing unload work must not be less than 0, got ${_work}`)
}, unblockUnload = () => {
	// work == 1; unblock unload
	// work >  1; don't unblock unload
	--_work || removeEventListener(
		'beforeunload',
		blockUnload,
		{capture: true}
	) || bkattach()
	console.assert(_work >= 0, `popping unload work must be greater than or equal to 0, got ${
		_work}`)
}



const upgradeKKMDb: ((u: UpgradeDB) => void) = u => {
	const kmap = u.createObjectStore<keyBindingsIDB, string>('key-mappings', {keyPath: 'code'})
	kmap.createIndex('code', 'code', {unique: true})
	kmap.createIndex('info', 'info', {unique: false}) // it's an Object but uh
	const dd: {[k: string]: number[]}= {
		KeyW: [ACTIONS.SCROLL_NY], // neg y
		KeyS: [ACTIONS.SCROLL_PY], // pos y
		KeyA: [ACTIONS.SCROLL_NX, ACTIONS.CH_NEXT], // neg x, next ch
		KeyD: [ACTIONS.SCROLL_PX, ACTIONS.CH_PREV], // pos x, prev ch
		KeyJ: [ACTIONS.SCROLL_PY], // pos y
		KeyK: [ACTIONS.SCROLL_NY], // neg y
		KeyL: [ACTIONS.SCROLL_PX], // pos x
		KeyH: [ACTIONS.SCROLL_NX], // neg x
		KeyN: [ACTIONS.PAGE_NEXT], // next page
		KeyP: [ACTIONS.PAGE_PREV], // prev page
	}
	const bc = bhm()
	for (const code in dd) {
		dd[code].length = 16
		const info = new Uint8Array(dd[code])
		kmap.put({code, info}, code)
		let i = dd[code].length
		while (i--) info[i] &&
			bc.postMessage({code, mod: i, action: info[i]})
	}
}
const getKeyCodeMap: ((block?: boolean) => Promise<DB>) = block => {
	const o = openDb('key-code-map', 1, upgradeKKMDb)
	if (block) {
		// wait until we can reliably retrieve data to clear old crap,
		// even if we're a little out-of-sync
		unloadBlock()
		bkdetatch()
		o.finally(unblockUnload)
	}
	return o
}
const setBinds = async () => {
	// wait until we can reliably retrieve data to clear old crap,
	// even if we're a little out-of-sync
	bkdetatch()
	const db = await getKeyCodeMap()
	const binds = await db
	.transaction('key-mappings', 'readonly')
	.objectStore<keyBindingsIDB, string>('key-mappings')
	.getAll()
	for (const code in keyCodeBindings)
		delete keyCodeBindings[code]
	// set selected options
	for (const {code, info} of binds)
		keyCodeBindings[code] = info
	bkattach()
}


document.addEventListener('resume', setBinds)
setBinds();

export interface setCode {(code: string, mod: number, action: ACTIONS): Promise<boolean>;}
export const setCode: setCode = async (c, m, a) => {
	unloadBlock()
	const kmap = await getKeyCodeMap(true)
	if (a !== 0 && !(c in keyCodeBindings))
		keyCodeBindings[c] = new Uint8Array(16);
	if (c in keyCodeBindings) {
		const tc = kmap.transaction('key-mappings', 'readwrite')
		const os = tc.objectStore<keyBindingsIDB, string>('key-mappings');
		keyCodeBindings[c][m] = a;
		bhm().postMessage({ code: c, mod: m, action: a });
		if (a === 0 && keyCodeBindings[c].every(isZero)) {
			os.delete(c);
			delete keyCodeBindings[c];
		} else os.put({ code: c, info: keyCodeBindings[c] }, c);
		await tc.complete;
		unblockUnload()
		return true;
	}
	unblockUnload()
	return false;
}
