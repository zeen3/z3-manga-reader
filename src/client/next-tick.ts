const FNs = Array(1)
const ARGs = Array(1)
const emp: ReadonlyArray = Object.freeze([])
const {apply} = Reflect

let N = 0
let port1: MessagePort | null = null, port2: MessagePort | null = null

const ntp_in = r => nt(r)
export const ntp = () => new Promise(ntp_in)
export interface nt {
	<U>(fn: (...args: U[]) => void, ...args: U[]): number;
	(): Promise<void>;
}
export const nt: nt = (fn, ...args) => {
	if ('function' === typeof fn) {
		FNs.push(fn)
		ARGs.push(args)
		if (port1) port1.postMessage(++N)
		else throw new TypeError('port1 failed to instantiate')
		return N
	}
	if (fn === undefined) return new Promise(ntp_in)
	throw fn
}
export default nt
export const cnt = (id: number) => {
	if (!Number.isInteger(id)) throw new RangeError('id must be an integer')
	if (id < 1) throw new RangeError('id must not be less than 1')
	delete FNs[id]
	delete ARGs[id]
}
interface clone {<O>(o: O): Promise<O>}
export const clone: clone = o => new Promise(r => {
	FNs.push(r)
	ARGs.push(emp)
	if (port2) port2.postMessage({n: ++N, o})
	else throw new TypeError('port2 failed to instantiate')
})


const p2 = (m: MessageEvent) => {
	if (Number.isInteger(m.data)) {
		const fn = FNs[m.data], args = ARGs[m.data]
		if (fn && args) apply(fn, null, args)
	}
	delete FNs[m.data]
	delete ARGs[m.data]
}
const p1 = (m: MessageEvent) => {
	if ('object' === typeof m.data && Number.isInteger(m.data.n)) {
		const fn = FNs[m.data.n], args = [m.data.o]
		if (fn && args) apply(fn, null, args)
		delete FNs[m.data.n]
		delete ARGs[m.data.n]
	}
}
const setUpNT = () => {
	const mc = new MessageChannel
	port1 = mc.port1
	port2 = mc.port2
	mc.port1.onmessage = p1
	mc.port2.onmessage = p2
}
document.addEventListener('resume', setUpNT)
document.addEventListener('freeze', () => {
	port1 = null
	port2 = null
})
setUpNT()


const rafpwm = new WeakMap<Promise<DOMHighResolutionTimeStamp>, number>()
const rafpwmj = new WeakMap<Promise<DOMHighResolutionTimeStamp>, () => void>()
export const rafpc = (p: Promise<DOMHighResolutionTimeStamp>) => {
	const rq = rafpwm.get(p)
	const rqj = rafpwmj.get(p)
	if (rq) cancelAnimationFrame(rq)
	if (rqj) rqj(new Error('Cancelled.'))
}
export const rafp = (): Promise<DOMHighResolutionTimeStamp> => {
	let n, j
	const p = new Promise((R, J) => {
		n = requestAnimationFrame(r)
		j = J
	})
	rafpwmj.set(p, j)
	rafpwm.set(p, n)
	return p
}
