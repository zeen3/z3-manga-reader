interface localStorageDefaultKeys {
	'scroll-speed-mul-x': string;
	'scroll-speed-mul-y': string;
	[key: string]: string;
}
const localStorageDefaults: localStorageDefaultKeys = Object.freeze({
	'scroll-speed-mul-y': '0.94',
	'scroll-speed-mul-x': '0.91'
})
type localStorageUpdate = (value: string, old?: string | null) => void;
const localStorageUpdates: {[key: string]: localStorageUpdate} = {
	'scroll-speed-mul-y'(vy: string) {scrollmul[0] = parseFloat(vy)},
	'scroll-speed-mul-x'(vx: string) {scrollmul[1] = parseFloat(vx)}
}

export const getVar = (item: keyof localStorageDefaultKeys):string => {
	if ('string' !== typeof item)
		throw new SyntaxError(`getVar only accepts string types.`)
	let t = localStorage.getItem(item)
	if (item in localStorageDefaults)
		return t === null ? localStorageDefaults[item] : t

	throw new SyntaxError(`getVar: item ${item} is not in defaults`)
}


// const setVar = (item: string, value: any) => localStorage.setItem(item, String(value))
addEventListener('storage', e => {
	if (e.storageArea !== localStorage) return;
	if ('string' === typeof e.key && e.key in localStorageUpdates && e.key in localStorageDefaults)
		localStorageUpdates[e.key](e.newValue === null ? localStorageDefaults[e.key] : e.newValue, e.oldValue)

	if (e.key === null)
		for (const update in localStorageUpdates)
			if (localStorageUpdates.hasOwnProperty(update))
				localStorageUpdates[update](localStorageDefaults[update], e.oldValue)
})
const enum SCROLLER {
	PHRT_OFFSET = 0,
	PHRT_LENGTH = 1,
	SCROLLS_OFFSET = (PHRT_LENGTH << 3) + PHRT_OFFSET,
	SCROLLS_LENGTH = 4,
	SCROLLMUL_OFFSET = SCROLLS_OFFSET + SCROLLS_LENGTH + 4,
	SCROLLMUL_LENGTH = 2,
	IS_SCROLLING_OFFSET = SCROLLMUL_OFFSET + (SCROLLMUL_LENGTH << 2),
	IS_SCROLLING_LENGTH = 1,
	TOTAL_LENGTH = IS_SCROLLING_OFFSET + IS_SCROLLING_LENGTH,
}

interface Scroller {(hrt: DOMHighResTimeStamp): void;}

const scrollbuffer = new ArrayBuffer(SCROLLER.TOTAL_LENGTH),
	scrollhrt = new Float64Array(scrollbuffer, SCROLLER.PHRT_OFFSET, SCROLLER.PHRT_LENGTH),
	scrollmove = new Uint8ClampedArray(scrollbuffer, SCROLLER.SCROLLS_OFFSET, SCROLLER.SCROLLS_LENGTH),
	__scrollmove = new Uint32Array(scrollbuffer, SCROLLER.SCROLLS_OFFSET, 2),
	scrollmul = new Float32Array(scrollbuffer, SCROLLER.SCROLLMUL_OFFSET, SCROLLER.SCROLLMUL_LENGTH),
	scrolling = new Uint8ClampedArray(scrollbuffer, SCROLLER.IS_SCROLLING_OFFSET, SCROLLER.IS_SCROLLING_LENGTH),
	scroller:Scroller = hrt => {
		// raf loop when we're scrolling
		if (scrolling[0]) __scrollmove[1] = requestAnimationFrame(scroller)
		// reset if we're not scrolling
		else return __scrollmove[0] = 0, cancelAnimationFrame(__scrollmove[1]);

		// right - left, down - up
		const dx = scrollmove[1] - scrollmove[3],
			dy = scrollmove[0] - scrollmove[2];
		if (dx || dy) {
			const dt = Math.min(hrt > scrollhrt[0] ? hrt - scrollhrt[0] : 0, 106.4),
				scr = document.fullscreenElement ||
				document.scrollingElement
				|| document.body
			scr.scrollBy(
				dx * dt * scrollmul[1],
				dy * dt * scrollmul[0]
			);
		} else if (__scrollmove[0] === 0) scrolling[0] = 0
		scrollhrt[0] = hrt
	};

export {scrolling, scrollmul, scrollmove, __scrollmove, scroller, scrollhrt}

scrollmul[0] = parseFloat(getVar('scroll-speed-mul-y'));
scrollmul[1] = parseFloat(getVar('scroll-speed-mul-x'));

