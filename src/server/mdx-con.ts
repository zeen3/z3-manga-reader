// 
// import { connect as h2con, ClientHttp2Session as ch2s, constants as h2const } from 'http2'
// import { URL } from 'url'
// import { EventEmitter } from 'events'
// 
// const connections = new Map<string, ch2s>();
// const h2conopt = Object.freeze({
// 	maxSessionMemory: 32,
// 	maxHeaderListPairs: 512,
// 	maxOutstandingPings: 4,
// 	paddingStrategy: h2const.PADDING_STRATEGY_ALIGNED,
// 	peerMaxConcurrentStreams: 32,
// 	settings: Object.freeze({
// 		enablePush: false
// 	})
// })
// export const mkcon = (authority: URL | string):Promise<ch2s> => new Promise((res, rej) => {
// 	if ('string' === typeof authority) authority = new URL(authority)
// 	const origin = authority.origin
// 	const curcon = connections.get(origin)
// 	if (curcon) {
// 		if (curcon.connecting) return curcon.once('connect', v => res(v))
// 		else if (!curcon.closed) return curcon
// 		else connections.delete(origin)
// 	}
// 
// 	try {
// 		let here = false
// 		const con = h2con(authority, h2conopt, session => {here = true; res(session)}),
// 			cleanup = () => {
// 				if (here) {
// 					connections.delete(origin)
// 					here = false
// 				}
// 				con.close()
// 				con.unref()
// 			}
// 		connections.set(origin, con);
// 		con.once('error', err => {
// 			rej(err)
// 			console.error(err)
// 			cleanup()
// 		}).once('frameError', (ftype, ecode, sid) => {
// 			console.error(`frame error: ${ftype}; code: ${ecode}; id: ${sid}`)
// 			cleanup()
// 		}).once('goaway', (err, lsid, data) => {
// 			console.error(`goaway code ${err}; lsid: ${lsid}; data 0x${data && data.length ? data.toString('hex') : ''} "${data}"`)
// 			cleanup()
// 		})
// 			.once('close', cleanup)
// 			.setTimeout(60_000, cleanup)
// 		// timeout: 60 seconds
// 		return;
// 	} catch (e) {
// 		console.error(e)
// 		connections.delete(origin)
// 		process.nextTick(() => rej(e))
// 		return;
// 	}
// })
// 
// export const enum PR {
// 	UI = 4,
// 	RQ = 1,
// 	BG = 2,
// 	UIRQ = 6
// }
// type tovoid = () => void;
// type priority = PR.UI | PR.RQ | PR.BG;
// 
// const ee = ():EventEmitter => new EventEmitter;
// interface eee {
// 	addListener(event: string | symbol, listener: (...args: any[]) => void): this;
// 	on(event: string | symbol, listener: (...args: any[]) => void): this;
// 	once(event: string | symbol, listener: (...args: any[]) => void): this;
// 	prependListener(event: string | symbol, listener: (...args: any[]) => void): this;
// 	prependOnceListener(event: string | symbol, listener: (...args: any[]) => void): this;
// 	removeListener(event: string | symbol, listener: (...args: any[]) => void): this;
// 	off(event: string | symbol, listener: (...args: any[]) => void): this;
// 	emit(event: string | symbol, ...args: any[]): boolean;
// }
// export function add <T extends eee>(res: tovoid, rej: tovoid, timeout = Infinity, type: priority | PR.UIRQ = PR.BG, signal: T = ee(), p: priorities = Priorities):T {
// 	const ifn = isFinite(timeout)
// 	const clr = () => {
// 		rej();
// 		signal.off('abort', clr)
// 		clean(fn, type);
// 	}
// 	let to = ifn ? null : setTimeout(clr, timeout);
// 	const fn = ifn ?
// 		(
// 			type === PR.UIRQ ?
// 			() => {res(); if (to !== null) clearTimeout(to); clean(fn, 4, 1); clean(fn, 2, 1)} :
// 			() => {res(); if (to !== null) clearTimeout(to)}
// 		) :
// 		res;
// 	signal.once('abort', clr)
// 	switch (type) {
// 		case PR.UIRQ:
// 			p.rq.push(fn)
// 			p.ui.push(fn)
// 			break
// 		case PR.UI:
// 			p.ui.push(fn)
// 			break
// 		case PR.RQ:
// 			p.rq.push(fn)
// 			break
// 		case PR.BG:
// 		default:
// 			p.bg.push(fn)
// 			break
// 	}
// 	return signal
// }
// 
// export const clean = (fn: () => void, hint = 7, n = NaN, p: priorities = Priorities):boolean => {
// 	if (hint & PR.UI) {
// 		const idx = p.ui.indexOf(fn)
// 		if (idx !== -1) {
// 			p.ui.splice(idx, 1)
// 			return true
// 		}
// 		if (--n < 1) return false
// 	}
// 	if (hint & PR.RQ) {
// 		const idx = p.rq.indexOf(fn)
// 		if (idx !== -1) {
// 			p.rq.splice(idx, 1)
// 			return true
// 		}
// 		if (--n < 1) return false
// 	}
// 	let idx = p.bg.indexOf(fn)
// 	if (idx !== -1) {
// 		p.bg.splice(idx, 1)
// 		return true
// 	}
// 	if (--n < 1) return false
// 	idx = p.rq.indexOf(fn)
// 	if (idx !== -1) {
// 		p.rq.splice(idx, 1)
// 		return true
// 	}
// 	if (--n < 1) return false
// 	idx = p.ui.indexOf(fn)
// 	if (idx !== -1) {
// 		p.ui.splice(idx, 1)
// 		return true
// 	}
// 	return false
// }
// 
// export const go = (pri = Priorities) => { 
// 	const n = ++pri.gone & 7;
// 	if (n & PR.UI) {
// 		const fn = pri.ui.shift()
// 		if ('function' === typeof fn) {
// 			fn()
// 			return;
// 		}
// 	}
// 	if (n & PR.UIRQ) {
// 		const fn = pri.rq.shift()
// 		if ('function' === typeof fn) {
// 			fn()
// 			return;
// 		}
// 	}
// 	const fn = pri.bg.shift() || pri.rq.shift() || pri.ui.shift();
// 	if ('function' === typeof fn) {
// 		fn()
// 		return;
// 	}
// }
// 
// export interface priorities {
// 	ui: Array<tovoid>;
// 	rq: Array<tovoid>;
// 	bg: Array<tovoid>;
// 	gone: number;
// 	go(p?: priorities): void;
// 	add: typeof add;
// 	clean: typeof clean;
// }
// export const Priorities: priorities = {
// 	ui: [],
// 	rq: [],
// 	bg: [],
// 	gone: 0,
// 	go,
// 	add,
// 	clean
// }
// 
