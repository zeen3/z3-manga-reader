// 
// import { scrypt, ScryptOptions, timingSafeEqual } from 'crypto'
// import { homedir } from 'os'
// import { join as pjoin } from 'path'
// import { fopenp, rnd, salt as salty } from './utils'
// 
// const DIR = process.env.MANGADIR || `${homedir()}/.z3mdx`
// const scryptOpts: ScryptOptions = Object.freeze({
// 	N: 1024 << parseInt(process.env.MANGAPWCOST || '4'),
// 	r: parseInt(process.env.MANGAPWSIZE || '8'),
// 	p: parseInt(process.env.MANGAPWPARR || '1'),
// 	maxmem: parseInt(process.env.MANGAPWMEM || '32') * 4 ** 10
// })
// const keyLen = 96;
// const saltLen = salty.byteLength;
// const scryptLen = keyLen + saltLen;
// 
// const pwfd = fopenp(pjoin(DIR, 'pw.bin'))
// const uidk = fopenp(pjoin(DIR, 'keys.bin'), null, async fh => {
// 	await fh.writeFile(await rnd(Buffer.allocUnsafe(528)))
// 	return fh
// }).then(async fh => {
// 	const d = await fh.readFile()
// 	await fh.close()
// 	let n = 0
// 	return {
// 		md5: [
// 			d.slice(n, n += 16),
// 			d.slice(n, n += 16),
// 			d.slice(n, n += 16),
// 			d.slice(n, n += 16)
// 		],
// 		sha1: [
// 			d.slice(n, n += 20),
// 			d.slice(n, n += 20),
// 			d.slice(n, n += 20),
// 			d.slice(n, n += 20)
// 		],
// 		sha256: [
// 			d.slice(n, n += 32),
// 			d.slice(n, n += 32),
// 			d.slice(n, n += 32),
// 			d.slice(n, n += 32)
// 		],
// 		sha512: [
// 			d.slice(n, n += 64),
// 			d.slice(n, n += 64),
// 			d.slice(n, n += 64),
// 			d.slice(n, n += 64)
// 		],
// 		n
// 	}
// })
// 
// const hash = (
// 	pw: string | Promise<string>,
// 	salt: Buffer | NodeJS.TypedArray | string | Promise<Buffer | NodeJS.TypedArray | string> = rnd()
// ): Promise<Buffer> => new Promise(async (res, rej) => scrypt(
// 	String(await pw).normalize('NFC'),
// 	await salt,
// 	keyLen,
// 	scryptOpts,
// 	async (err, buf) => {
// 		if (err) rej(err)
// 		else res(buf)
// 	}
// ))
// 
// const compare = (
// 	to: Buffer,
// 	pw: string | Promise<string>
// ):Promise<boolean> => hash(pw, to.slice(0, saltLen))
// 	.then(buf => timingSafeEqual(
// 		to.slice(saltLen, scryptLen),
// 		buf
// 	))
// 
// // const getuid = async (uname: string) => {
// // 	const fh = await pwfd
// // }
// 
// export { pwfd, scryptOpts, keyLen, saltLen, scryptLen, compare, hash }
