// 
// import { promises as fs, constants as fc } from 'fs'
// import { dirname } from 'path'
// import { randomFill } from 'crypto'
// import { TextEncoder, TextDecoder } from 'util'
// 
// export const mkdirp = (d: string, p = 0o777 ^ process.umask()):Promise<void> => fs.mkdir(d, p)
// 	.catch(e => 'code' in e && e.code === 'ENOENT' ?
// 		mkdirp(dirname(d), p).then(() => fs.mkdir(d, p)) :
// 		Promise.reject(e)
// 	);
// export const fopenF = fc.O_RDWR | fc.O_DSYNC | fc.O_NONBLOCK | fc.O_CREAT;
// export const fopenp = (file: string, flags: null | number = null, create: ((fh: fs.FileHandle) => void) = () => null): Promise<fs.FileHandle> => fs.open(file,
// 	flags === null ?
// 	fopenF :
// 	flags
// ).catch(async e => {
// 	if (!('code' in e && e.code === 'ENOENT'))
// 		throw e
// 	await mkdirp(dirname(file))
// 	return fs.open('path' in e && 'string' === typeof e.path ? e.path : file, (flags === null ? fopenF : flags) | fc.O_EXCL)
// 		.then(async fh => {
// 			await create(fh)
// 			await fh.datasync()
// 			await fh.close()
// 			return fopenp(file, flags, () => Promise.reject())
// 		})
// })
// 
// export const rnds = Buffer.alloc(64)
// export const salt = Buffer.from(rnds.buffer, rnds.byteOffset, 32)
// export const rnd = <T extends Buffer | NodeJS.TypedArray>(
// 	buf: T = salt,
// 	offset = 0,
// 	size = buf.byteLength - offset
// ):Promise<T> => new Promise((r, j) => randomFill(
// 	buf, offset, size, (e, b) => e ? j(e) : r(b)
// ))
// 
// export const encoder = new TextEncoder;
// export const decoder = new TextDecoder;
