import {openDb, Cursor} from './node_modules/idb/lib/idb'
import {ACTIONS, keyBindingsIDB} from '../client/actions'
keymapper: {
    let keymanjs: undefined | File;
    const keyworks: [string /* code */, number /* mod */, number /* act */][] = [];
    /*
    JSON.stringify(keyworks.reduce((o, kw) => {
        if (!(kw[0] in o)) o[kw[0]] = new Uint8Array(16)
        o[kw[0]][kw[1]] = kw[2]
        return o
    }, {} as keyBindings), (k: any, v: any) => {
        if (v instanceof Uint8Array)
        return Array.from(v)
        if (k === '') return v
        return v
    })
    */
    let doStringify: number = 0
    const stringify = () => caches.open('keyman').then(man => man
        .put('/keyman.js', new Response(keymanjs = new File([
            'var BINDINGS = ',
            JSON.stringify(keyworks),
            ';'
        ], 'keyman.js', {type: 'application/javascript'}), {
            headers: new Headers([
                ['content-type', 'application/javascript'],
                ['content-length', String(keymanjs.size)]
            ])
        }))).finally(() => { doStringify = 0 })

    addEventListener('fetch', (e: FetchEvent) => {
        let u = new URL(e.request.url)
        if (u.pathname === '/keyman.js') {
            if (keymanjs) {
                const p = new Response(keymanjs, {
                    headers: new Headers([
                        ['content-type', 'application/javascript'],
                        ['content-length', String(keymanjs.size)],
                    ])
                })
                e.respondWith(Promise.resolve(p))
            } else {
                e.respondWith(caches.match('/keyman.js').then(res => res ||
                    new Response('')))
            }
        }
    })
    new BroadcastChannel('key-code-map-handle').onmessage = m => {
        const {code, mod, action} = m.data
        if ('string' === typeof code &&
            'number' === typeof mod && Number.isInteger(mod) && 
            'number' === typeof action && Number.isInteger(action)
        ) keyworks.push([code, mod, action])
        else {return;}
        doStringify || (doStringify = setTimeout(stringify, 33))
    }

    openDb('key-code-map', 1, u => {
        const kmap = u.createObjectStore('key-mappings', {keyPath: 'code'})
        kmap.createIndex('code', 'code', {unique: true})
        kmap.createIndex('info', 'info', {unique: false}) // it's an Object but uh
        const dd: {[k: string]: number[]} = {
            KeyW: [ACTIONS.SCROLL_NY], // neg y
            KeyS: [ACTIONS.SCROLL_PY], // pos y
            KeyA: [ACTIONS.SCROLL_NX, ACTIONS.CH_NEXT], // neg x, next ch
            KeyD: [ACTIONS.SCROLL_PX, ACTIONS.CH_PREV], // pos x, prev ch
            KeyJ: [ACTIONS.SCROLL_PY], // pos y
            KeyK: [ACTIONS.SCROLL_NY], // neg y
            KeyL: [ACTIONS.SCROLL_PX], // pos x
            KeyH: [ACTIONS.SCROLL_NX], // neg x
            KeyN: [ACTIONS.PAGE_NEXT], // next page
            KeyP: [ACTIONS.PAGE_PREV], // prev page
        }
        const bc = new BroadcastChannel('key-code-map-handle')
        for (const code in dd) {
            dd[code].length = 16
            const info = new Uint8Array(dd[code])
            kmap.put({code, info}, code)
            let i = dd[code].length
            while (i--) info[i] &&
                bc.postMessage({code, mod: i, action: info[i]})
        }
    }).then(async db => {
        let cur: undefined | Cursor<keyBindingsIDB, string> = await db
        .transaction('key-mappings', 'readonly')
        .objectStore('key-mappings')
        .openCursor()
        while (cur) {
            const v = cur.value.info
            let i = 16
            while (i--) v[i] && keyworks.push([cur.value.code, i, v[i]])
            cur = await cur.continue()
        }
        doStringify || (doStringify = setTimeout(stringify, 1))
    })

}
